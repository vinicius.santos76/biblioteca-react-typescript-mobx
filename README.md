# Projeto Liber

## Critério Norteadores

### Garantir que o site:

Conte com filtros por gênero literário

Conte com uma barra de busca de livros

## Desenvolvimento

### Ferramentas utilizadas no desenvolvimento:

React

React Autocomplete

Typescript

Mobx

Material UI

Docker

Git

Webpack

## Executando projeto

### Utilizando Docker Desenvolvimento  - localhost:3500

1. npm install

2. npm run containerDev

### Utilizando Docker Produção  - localhost:80

1. npm install

2. npm run containerNginx

### Sem utilização de Docker - localhost:3000

1. npm install

2. npm start
