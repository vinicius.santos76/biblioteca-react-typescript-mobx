import * as React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import HeaderBiblioteca from './componentes/HeaderBiblioteca'
import SideBar from './componentes/SideBar'
import ContainerLivros from './componentes/ContainerLivros'
import DevTools from 'mobx-react-devtools'
import { Paper, Snackbar, createStyles } from '@material-ui/core';
import { inject, observer } from 'mobx-react';
import Store from './componentes/store/Store';

const Style = createStyles({
  Container: {
    minWidth: '100%',
    minHeight: '100%',
    position: 'absolute',
    backgroundColor: '#f3f3f3',
    overflowX: 'hidden'
  }
})

interface InnerProps {
  StoreApp: Store
}

@inject("StoreApp")
@observer
class App extends React.Component<InnerProps> {

  public render() {
    return (
      <Paper style={Style.Container}>
        <CssBaseline /> {/*Reseta estilo do CSS para maior compatibilidade entre navegadores*/}
        <HeaderBiblioteca /> {/*Cabeçalho*/}
        <SideBar />
        <ContainerLivros />
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={this.props.StoreApp.SnackBarkOpened}
          onClose={this.props.StoreApp.handleClickCloseSnack()}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{this.props.StoreApp.SnackBarMessage}</span>}
        />
        <DevTools />
      </Paper>
    );
  }

}

export default App as React.ComponentType<{}>;
