import * as React from 'react'
import { Paper, Checkbox, Table, TableRow, TableCell, Icon, Button, Dialog, TableBody, Typography, createStyles, AppBar, Toolbar, IconButton } from '@material-ui/core';
import * as Generos from '../assets/json/Generos.json';
import ShoppingCart from '@material-ui/icons/ShoppingCart'
import RelatorioCompra from './RelatorioCompra'
import { inject, observer } from 'mobx-react';
import Store from './store/Store';
import CloseIcon from '@material-ui/icons/Close';

const Style = createStyles({
    PaperContainer: {
        height: '100%',
        position: 'fixed',
        width: 200,
        marginTop: 78,
        padding: 5
    },
    Table: {
        marginTop: 15
    }
})

interface InnerProps {
    StoreApp: Store
}

@inject("StoreApp")
@observer
class SideBar extends React.Component<InnerProps> {


    // Abre caixa de diálogo
    public handleOpenDialog = () => {
        this.props.StoreApp.DialogComprasOpened = true
    }

    // Fecha caixa de diálogo
    public handleCloseDialog = () => {
        this.props.StoreApp.DialogComprasOpened = false
    }

    /* Verifica o valor do checkbox, adiciona ou retira o genero do array de generos selecionados dependendo
         do resultado e aciona função de filtragem */

    public HandleCheckbox = (genero: string) => (event: React.ChangeEvent<any>) => {
        
        // Caso o checkbox seja checado irá adicionar o genero ao array de generos selecionados, caso não irá remove-lo
        if (event.target.checked) {
           
            this.props.StoreApp.GenerosSelecionados = this.props.StoreApp.GenerosSelecionados.concat(genero)
        } else {
            // Verifica qual a posição do genero selecionado dentro da lista de generos e se não existir retorna -1
            const indiceGenero: number = this.props.StoreApp.GenerosSelecionados.indexOf(genero)

            // Remove o indice desejado do Array e seta valor GenerosSelecionados da Store
            this.props.StoreApp.GenerosSelecionados.splice(indiceGenero, 1)
        }

        this.props.StoreApp.FilterLivrosGenero()
    }

    public render() {
        const GenerosCheckBox = Generos.generos.map((TextoGenero: string, index: number) => {
            return (
                <TableRow key={index}>
                    <TableCell
                        padding="checkbox"
                    >
                        <Typography
                            variant="subheading"
                        >
                            {TextoGenero}
                        </Typography>
                    </TableCell>
                    <TableCell
                        padding="checkbox"
                    >
                        <Checkbox
                            key={index}
                            color='primary'
                            // defaultChecked={this.state.CheckboxChecked}
                            // tslint:disable-next-line:jsx-no-lambda
                            onClick={this.HandleCheckbox(TextoGenero)}
                        />
                    </TableCell>
                </ TableRow>

            )
        }
        )
        return (
            <Paper style={Style.PaperContainer}>
                <Button
                    onClick={this.handleOpenDialog}
                    variant="contained"
                    name="Exibir carrinho"
                    color="primary"
                >
                    <Icon>
                        <ShoppingCart />
                    </Icon>
                    Finalizar Compra
                </Button>
                <Table
                    style={Style.Table}
                >
                    <TableBody>
                        {GenerosCheckBox}
                    </TableBody>
                </Table>
                <Dialog
                    fullScreen={true}
                    open={this.props.StoreApp.DialogComprasOpened}
                >
                    <AppBar >
                        <Toolbar>
                            <IconButton color="inherit" onClick={this.handleCloseDialog} aria-label="Close">
                                <CloseIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <RelatorioCompra />
                </Dialog>
            </Paper>
        )
    }
}

export default SideBar as React.ComponentType<{}>; 