import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { Card, Typography, Icon, Button, Snackbar, Dialog, ButtonBase, createStyles } from '@material-ui/core';
import AddShoppingCart from '@material-ui/icons/AddShoppingCart'
import DetalhesLivro from './DetalhesLivro'
import Store from './store/Store';

const Style = createStyles({
     styleCardLivro: {
        padding: 5,
        width: 180,
        margin: 12,
        display: 'grid',
    },

     styleImgLivro:  {
        width: '100%',
        height: 240,
        padding: 5,
        display: 'block',
    },

     tipoTitulo:  {
        fontSize: 20,
    },

     StyleTextButton:  {
        fontSize: 16,
        float: 'right',
        marginLeft: 20,
    },
    StyleQtdCompra: {
        padding: 10,
        borderRadius: '100%',
        // display: QuantidadeVendidos > 0 ? 'block': 'none',
        backgroundColor: 'red',
        color: '#fff',
        position: 'absolute',
        marginLeft: 160,
        marginTop: -5,
        zIndex: 2
    }
})

/*Interface exportada para uso nos demais componentes*/
export interface ILivros {
    TituloLivro: string,
    Autor: string
    Conteudo: string
    Imagem: string
    Valor: string,
    Genero: string
}

interface InnerProps {
    StoreApp: Store
}

interface InnerState {
    NroCompra: number,
    SnackBarkOpened: boolean,
    SnackBarMessage: string,
    DialogOpened: boolean
}

@inject("StoreApp")
@observer
class Livro extends React.Component<ILivros & InnerProps, InnerState>
{

    public state = {
        NroCompra: 0,
        SnackBarkOpened: false,
        SnackBarMessage: "",
        DialogOpened: false
    }

    private handleClickOpenDialog = () => {
        this.setState({ DialogOpened: true });
    };

    private handleClickCloseDialog = () => {
        this.setState({ DialogOpened: false });
    };


    public handleClickOpenSnack = () => {
        this.setState({ SnackBarkOpened: true });
    };

    public handleClickCloseSnack = () => {
        this.setState({ SnackBarkOpened: false })
    }


    public render() {

        // Cria um array para retornar todos os livros vendidos com o titulo do livro deste componente
        const ArrayVendidos: Array<ILivros> = this.props.StoreApp.ArrayLivrosCarrinho.filter((item: ILivros, index: number, ArrayOriginal: ILivros[]) => {
            return item.TituloLivro === this.props.TituloLivro
        })

        const QuantidadeVendidos = ArrayVendidos.length

        // Imagem importada dinamicamente com Require - Não suportado por Material-UI CardMedia
        const imgLivro = require('../assets/images/' + this.props.Imagem)

        return (
            <Card style={Style.styleCardLivro} elevation={0}>

                <div style={{...Style.StyleQtdCompra,display: QuantidadeVendidos > 0 ? 'block': 'none'}}>
                    {QuantidadeVendidos}
                </div>

                <ButtonBase
                    onClick={this.handleClickOpenDialog}
                >
                    <img
                        src={imgLivro}
                        alt={this.props.TituloLivro}
                        style={Style.styleImgLivro}
                    />
                </ ButtonBase>

                <Button
                    onClick={this.props.StoreApp.AddItemCarrinho(this.props)}
                    variant='contained'
                    fullWidth={true}
                    name="Add Livro"
                    color='secondary'
                >
                    <Icon color="primary">
                        <AddShoppingCart />
                    </Icon>
                    <Typography style={Style.StyleTextButton}>
                        R$ {this.props.Valor}
                    </ Typography>
                </Button>

                <Snackbar
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    open={this.state.SnackBarkOpened}
                    onClose={this.handleClickCloseSnack}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{this.state.SnackBarMessage}</span>}
                />
                <Dialog
                    open={this.state.DialogOpened}
                    onClose={this.handleClickCloseDialog}
                >
                    <DetalhesLivro
                    
                        // Passa os valores de props para os DetalhesLivro
                        {...this.props}
                    />
                </Dialog>
            </Card>
        )
    }
}

export default Livro as React.ComponentType<ILivros>;