import * as React from 'react'
import { Paper, Typography, Table, TableBody, TableRow, TableCell, TableHead, Button, Icon, createStyles, Card } from '@material-ui/core';
import { inject, observer } from 'mobx-react';
import Store from './store/Store';
import { ILivros } from './Livro';
import RemoveShoppingCart from '@material-ui/icons/RemoveShoppingCart'

const Style = createStyles({
    StyleContainer: {
        marginTop: 70
    },
    StyleImgLivro: {
        height: 100
    },
    TextTotais:{
        padding:20,
        marginLeft: 25
    }
})

interface InnerProps {
    StoreApp: Store
}

@inject("StoreApp")
@observer
class RelatorioCompra extends React.Component<InnerProps>{

    public render() {



        const ItensCompra = this.props.StoreApp.LivrosUnicos.map((obj: ILivros, indice: number) => {

            // Cria um array para retornar todos os livros vendidos com o titulo do livro deste componente
            const ArrayVendidos: Array<ILivros> = this.props.StoreApp.ArrayLivrosCarrinho.filter((item: ILivros, index: number, ArrayOriginal: ILivros[]) => {
                return item.TituloLivro === obj.TituloLivro
            })

            const QuantidadeVendidos = ArrayVendidos.length
            // Importa imagem dinamicamente, não suportado por CardMedia
            const ImgLivro = require('../assets/images/' + obj.Imagem)

            return (
                <TableRow key={indice}>
                    <TableCell>
                        <img src={ImgLivro} style={Style.StyleImgLivro} />
                    </TableCell>
                    <TableCell>
                        <Typography
                            variant="subheading"
                        >
                            {obj.TituloLivro}
                        </Typography>
                    </TableCell>
                    <TableCell>
                        <Typography
                            variant="subheading"
                        >
                            {QuantidadeVendidos}
                        </Typography>
                    </TableCell>
                    <TableCell>
                        <Typography
                            variant="subheading"
                        >
                            {(Number(obj.Valor) * QuantidadeVendidos).toFixed(2)}
                        </Typography>
                    </TableCell>
                    <TableCell>
                        <Typography
                            variant="subheading"
                        >
                            <Button
                                onClick={this.props.StoreApp.RemoveItemCarrinho(obj)}
                                variant='contained'
                                fullWidth={true}
                                name="Remove"
                            >
                                <Icon color="primary">
                                    <RemoveShoppingCart />
                                </Icon>

                            </Button>
                        </Typography>
                    </TableCell>
                </TableRow>
            )
        })

        return (
            <Paper
                elevation={0}
                style={Style.StyleContainer}
            >
                {/* Tabela com itens comprados */}
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell
                                colSpan={2}
                            >
                                <Typography
                                    variant="title"
                                >
                                    Livro
                                </Typography>
                            </TableCell>
                            <TableCell
                                colSpan={1}
                            >
                                <Typography
                                    variant="title"
                                >
                                    Quantidade
                                </Typography>
                            </TableCell>
                            <TableCell>
                                <Typography
                                    variant="title"
                                >
                                    Valor
                                </Typography>
                            </TableCell>
                            <TableCell>
                                <Typography
                                    variant="title"
                                >
                                    Remover
                                </Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {ItensCompra}
                    </TableBody>
                </Table>

                {/* Tabela com totais da compra */}
                <Card
                    elevation={0}
                    style={{
                        display: 'flex',
                        flexWrap: 'wrap',
                    }}
                >
                    <Typography
                        variant="subheading"
                        style={Style.TextTotais}
                    >
                        Desconto: {this.props.StoreApp.getDesconto * 100} %
                                </Typography>

                    <Typography
                        paragraph={false}
                        style={{...Style.TextTotais, textDecoration: 'line-through'}}
                    >
                        R${this.props.StoreApp.ValorTotalCompras.toFixed(2)}
                    </Typography>


                    <Typography
                        variant="subheading"
                        paragraph={false}
                        style={Style.TextTotais}
                    >
                        Total R$ {(this.props.StoreApp.ValorTotalCompras * (1 - this.props.StoreApp.getDesconto)).toFixed(2)}
                    </Typography>
                </Card>

            </Paper>
        )
    }
}

export default RelatorioCompra as React.ComponentType<{}>;