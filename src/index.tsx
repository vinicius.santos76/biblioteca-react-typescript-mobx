import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'mobx-react';
import Store from './componentes/store/Store';
// Material UI 
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import red from '@material-ui/core/colors/red';

// Cores Themas da aplicação
const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: {
      main: '#fff'
    },
    error: red,
  }

});

// Store Mobx 
const StoreApp = new Store()

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider StoreApp={StoreApp}>
      <App />
    </Provider>
  </ MuiThemeProvider>
  ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
